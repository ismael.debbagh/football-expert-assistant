Le Stade brestois 29, couramment appelé Stade brestois, est un club de football français situé à Brest et fondé en 1950 à la suite de la fusion de cinq patronages locaux, dont l'Armoricaine de Brest, fondé en 1903.

Dans ses premières années, le club réalise une montée rapide dans la hiérarchie du football amateur, au point d'être promu en championnat de France amateurs, premier échelon du football amateur français, en 1958. Le club intègre la deuxième division en 1970, puis découvre la première division en 1979. Il connaît son apogée sportif entre 1981 et 1991, sous la présidence de François Yvinec, en disputant neuf saisons dans l'élite en dix ans.

En 1991, le club est sanctionné par la DNCG d'une rétrogradation en seconde division en raison d'un déficit considérable puis dépose le bilan quelques mois plus tard. Le club repart au niveau amateur et ne renoue avec le professionnalisme que lorsqu'il remonte en deuxième division en 2004. En 2010, il retrouve le plus haut niveau, la Ligue 1, dix-neuf ans après l'avoir quitté.

Le club brestois est présidé, depuis le 10 mai 2016, par l'entrepreneur Denis Le Saint et l'équipe première, qui évolue au sein du championnat de France de Ligue 1, est dirigée par Éric Roy depuis le 3 janvier 2023.

À l'issue de la saison 2018-2019, le club est promu en Ligue 1 pour la première fois depuis 2010 en finissant vice-champion, juste derrière le FC Metz.

À la suite de la saison 2018-2019 en Ligue 2, le club finit vice-champion tout juste derrière le FC Metz à la 37e journée, après avoir vaincu Niort à domicile sur une large victoire 3-0. Le Stade brestois est ainsi promu pour la saison suivante en Ligue 1, qu'il retrouve pour la première fois depuis 2013. Par ailleurs, Gaëtan Charbonnier finit meilleur buteur de la saison avec 27 buts, un record pour le club depuis 1979.

Le Stade brestois termine 14e de Ligue 1 lors de la saison 2019-2020, interrompue en raison de la pandémie de Covid-19. Le Stade, entraîné par Olivier Dall'Oglio, a remporté 8 victoires, signé 10 nuls et concédé 10 défaites. Il a été éliminé en 32e de finale de Coupe de France et en quart de finale de Coupe de la Ligue20.

La saison 2020-2021 est plus compliquée. Le Stade Brestois se sauve in extremis lors de la dernière journée du championnat grâce à une victoire de Montpellier à Nantes. Les Ty Zefs terminent 17e avec 41 points (11 victoires, 8 nuls, 19 défaites)21. A l'issue de la saison, Olivier Dall'Oglio échange son poste avec son homologue Michel Der Zakarian de Montpellier qui deviendra le nouvel entraîneur de Brest.

La saison 2021-2022, débute mal pour le club qui doit attendre la 12e journée pour glaner sa première victoire avant d'enchaîner une suite de 6 victoires de rang historique. Les protégés de Michel Der Zakarian finiront à une très honorable 11e place (48 points) à l'issue de cette saison, qui verra le club fêter ses 70 ans lors de la 38e journée en présence d'anciennes gloires du club. (Anniversaire qui devait être célébré lors de la saison précédente mais gâché par la pandémie de Covid 19).

Lors de la saison 2022-2023, le 28 août 2022, le Stade Brestois connaît sa plus large défaite à domicile de son histoire en encaissant un 0-7, par Montpellier HSC, entraîné par Olivier Dall'Oglio ancien coach du club brestois22,23.

Le 11 octobre 2022, le renvoi de Michel Der Zakarian est annoncé après un mauvais début de saison le club finistérien est classé 20e de Ligue 1 au bout de 10 journées, l'intérim sera assurée par le capitaine historique du club, Bruno Grougi assisté de Julien Lachuer et Yvan Bourgis jusqu'à la nomination d'un entraineur principal24 (Bilan du trio 2 victoires, 1 nul, 4 défaites.)

Le 3 janvier 2023, c'est Eric Roy qui est nommé entraineur de l'équipe première jusqu'à la fin de la saison, il aura pour tâche de maintenir le club dans l'élite du football Français.