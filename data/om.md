L'Olympique de Marseille (OM) est un club de football français fondé à Marseille le 31 août 1899 par René Dufaure de Montmirail.

Le club joue au stade de l'Huveaune de 1904 jusqu'en 1937, date à laquelle est inauguré le stade Vélodrome. Auparavant, l'entité olympienne remporte sa première Coupe de France en 1924 et devient le tout premier club provincial à s'adjuger ce titre. L'OM fait partie des clubs ayant joué la saison inaugurale 1932-1933 et qui évoluent encore en Ligue 1 aujourd'hui. Après un premier titre de champion de France en 1929, dans une compétition aujourd'hui disparue, le club phocéen remporte son premier championnat professionnel en 1937, avant d'être rétrogradé pour la première fois en deuxième division en 1959. Une série de montées et de descentes s'effectue dans les années 1960. La décennie suivante, l'OM réalise en 1972 le premier doublé Coupe-Championnat de son histoire.

Après une période noire au début des années 1980 où l'institution marseillaise manque de faire faillite, l'arrivée de Bernard Tapie coïncide avec la période la plus faste du club, qui remporte quatre titres de champion consécutifs, une Coupe de France et la Ligue des champions 1992-1993. Ce titre reste la première et unique C1 remportée par un club français, ainsi que la première Ligue des champions du nom.

L'affaire VA-OM et ses conséquences économiques plongent le club en deuxième division. À la suite de sa remontée en 1996, le club de la cité phocéenne se confronte à nouveau à la justice avec l'affaire des comptes de l'OM et n'arrive plus à gagner de titre majeur, malgré notamment deux finales de Coupe UEFA (1999 et 2004). Cette période blanche prend fin avec un titre de champion de France en 2010 et trois victoires consécutives en Coupe de la Ligue (2010, 2011 et 2012).

L'Olympique de Marseille est membre ordinaire de l'association européenne des clubs (ECA), groupement des plus grands clubs européens issu de la dissolution du G14 en 20084.

Le club est présidé par Pablo Longoria depuis le 26 février 2021, l'actionnaire majoritaire étant l'Américain Frank McCourt. L'équipe première, qui dispute ses rencontres à domicile à l'Orange Vélodrome, est entraînée par Jean-Louis Gasset pour le compte de la saison 2023-2024 de Ligue 1.

