import sys

import boto3 # librairie permettant d'intéragir avec les services AWS
from botocore.config import Config

import collections

from langchain_community.chat_models import BedrockChat
from langchain_community.embeddings import HuggingFaceEmbeddings

from langchain.chains import ConversationChain, RetrievalQA
#from langchain.memory import ConversationBufferMemory
from langchain.prompts import PromptTemplate
from langchain_core.output_parsers import StrOutputParser

from load_db_from_folder import DataLoader

sys.path.append('../')
from config import (CREDENTIALS_PROFILE_NAME, REGION_NAME, MODEL_ID, 
                    MAX_TOKENS, TEMPERATURE, TOP_K, TOP_P)


class FootballExpertChatbot():
    def __init__(self, new_db = True):
        self.credentials_profile_name = CREDENTIALS_PROFILE_NAME
        self.region_name = REGION_NAME

        self.new_db = new_db
        self.embeddings = self.get_embeddings()

        self.model_id = MODEL_ID
        self.max_tokens = int(MAX_TOKENS)
        self.temperature = float(TEMPERATURE)
        self.top_k = int(TOP_K) # mettre à 1 ou 2
        self.top_p = float(TOP_P) # très petit ex: 0.1
        self.model = self.create_model()
        self.num_chat = 0

        self.conversation_memory = """
        Système : Tu es un assistant qui peut répondre à des questions liées au football. Ton nom est Footy.
        """

        if self.new_db:
            self.db = DataLoader().set_db(self.embeddings)
        else:
            self.db = DataLoader().get_db(self.embeddings)

        self.retriever = self.db.as_retriever()

    def config_aws_session(self):
        retry_config = Config(
            region_name = self.region_name,
            retries = {
                'max_attempts': 10,
                'mode': 'standard'
            }
        )

        session = boto3.session.Session(profile_name=self.credentials_profile_name)
        boto3_bedrock_runtime = session.client("bedrock-runtime", config=retry_config)

        return boto3_bedrock_runtime

    def create_model(self):
        boto3_bedrock_runtime = self.config_aws_session()

        model_kwargs =  { 
            "max_tokens": self.max_tokens,  # Claude-3 uses “max_tokens” however Claud-2 requires “max_tokens_to_sample”.
            "temperature": self.temperature, # Contrôle créativité de la génération du texte
            "top_k": self.top_k, # Un paramètre qui contrôle la diversité de la génération en limitant le choix des jetons aux K jetons les plus probables
            "top_p": self.top_p, # Un paramètre qui contrôle la diversité de la génération en limitant le choix des jetons aux jetons dont la somme de probabilité cumulée est supérieure à p
            "stop_sequences": ["\n\nHuman"],
        }

        model = BedrockChat(
            client=boto3_bedrock_runtime,
            model_id=self.model_id,
            model_kwargs=model_kwargs,
        )

        return model
    
    def get_embeddings(self):
        embeddings = HuggingFaceEmbeddings(model_name="all-MiniLM-L6-v2")

        return embeddings
    
    def get_template(self, conversation_memory):
        prompt = f"""
        Voici l'historique de la conversation :
        -----
        {conversation_memory}
        -----
        """

        prompt = prompt + "\n" + """
        En te basant sur les extraits suivants :
        -----
        {context}
        -----
        Réponds à la question suivante :
        Question: {question}
        Réponse :
        """

        return prompt
    
    def get_prompt(self) -> PromptTemplate:
        template = self.get_template(self.conversation_memory)
        prompt = PromptTemplate(
            template=template,
            input_variables=["context", "question"]
        )
        return prompt
    
    def upload_conversation_memory(self, last_question, last_answer):
        memory = self.conversation_memory + "\n" + f"""
        Utilisateur : {last_question}
        AI : {last_answer}
        """

        return memory
    
    def get_retrieval_qa(self, prompt): # Initialise la chaîne de q&a
        chain_type_kwargs = {"prompt": prompt}
        qa = RetrievalQA.from_chain_type(
            llm=self.model,
            chain_type="stuff",
            retriever=self.retriever,
            return_source_documents=True,
            chain_type_kwargs=chain_type_kwargs
        )

        return qa

    def retrieval_qa_inference(self, question, verbose=False): # Lance la châine de q&a à partir d'une question
        prompt = self.get_prompt()

        query = {"query": question}
        print("PROMPT ", self.num_chat , " :\n", prompt)
        answer = self.get_retrieval_qa(prompt)(query)
        print("ANSWER :\n", answer)
        print("type(answer) :\n", type(answer))
        sources = self.list_top_k_sources(answer, k=2)

        if verbose:
            print(sources)

        self.conversation_memory = self.upload_conversation_memory(question, answer["result"])

        self.num_chat += 1

        return answer["result"], sources

    def list_top_k_sources(self, answer, k=2):
        sources = [
            f'({res.metadata["source"]})'
            for res in answer["source_documents"]
        ]

        if sources:
            k = min(k, len(sources))
            distinct_sources = list(zip(*collections.Counter(sources).most_common()))[0][:k]
            distinct_sources_str = "  \n- ".join(distinct_sources)

        if len(distinct_sources) == 1:
            return f"Voici la source qui pourrait t'être utile :  \n- {distinct_sources_str}"

        elif len(distinct_sources) > 1:
            return f"Voici {len(distinct_sources)} sources qui pourraient t'être utiles :  \n- {distinct_sources_str}"

        else:
            return "Désolé je n'ai trouvé aucune ressource pour répondre à ta question"
        
    def format_response(self, response, sources):
        new_response = response + "\n\n" + sources

        return new_response
