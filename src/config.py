# Imports
# Env var
import os
import sys
from dotenv import load_dotenv, find_dotenv

# Env variables
sys.path.append('../..')
_ = load_dotenv(find_dotenv())

# AWS 
CREDENTIALS_PROFILE_NAME = os.environ['CREDENTIALS_PROFILE_NAME']
REGION_NAME = os.environ['REGION_NAME']

# Model
MODEL_ID = os.environ['MODEL_ID']
MAX_TOKENS = os.environ['MAX_TOKENS']
TEMPERATURE = os.environ['TEMPERATURE']
TOP_K = os.environ['TOP_K']
TOP_P = os.environ['TOP_P']

# Embedding
# Embedding
CHUNK_SIZE = os.environ['CHUNK_SIZE']
CHUNK_OVERLAP = os.environ['CHUNK_OVERLAP']

# Directories
PERSIST_DIRECTORY = str('./db/chroma/')
DATA_FOLDER = str('data')