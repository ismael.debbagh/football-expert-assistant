import warnings
warnings.filterwarnings("ignore")

import streamlit as st
from football_expert_chatbot import FootballExpertChatbot
from load_db_from_folder import DataLoader

# Load Model
@st.cache_resource # décorateur permettant à streamlit de conserver en mémoire une copie de la sortie de la fonction qui suit, lorsque get_model est appelé sa sortie sera mise dans le cahce et si elle est rappelée on n'aura pas beoin de recalculer la sortie
def get_model():
    model = FootballExpertChatbot(new_db=False)
    return model

model = get_model()

# Streamlit App
if "messages" not in st.session_state:
    st.session_state["messages"] = [{"role": "assistant", "content": "Posez moi des questions liées au football ?"}] # st.session_state permet de garder des variables dans l'état de la session pour ensuite pouvoir les réutiliser

for msg in st.session_state.messages:
    st.chat_message(msg["role"]).write(msg["content"])

if prompt := st.chat_input("Posez moi des questions liées au football ?"):
    # Add prompt
    st.session_state.messages.append({"role": "user", "content": prompt})
    st.chat_message("user").write(prompt)

    # Get answer
    response, sources = model.retrieval_qa_inference(prompt)

    # Add answer 
    st.chat_message("assistant").write(model.format_response(response, sources))
    st.session_state.messages.append({"role": "assistant", "content": response})