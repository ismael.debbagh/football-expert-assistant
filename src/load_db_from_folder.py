# Run the following code if you get an error about the nltk library ("Resource punkt not found.")
"""import nltk
import ssl

try:
    _create_unverified_https_context = ssl._create_unverified_context
except AttributeError:
    pass
else:
    ssl._create_default_https_context = _create_unverified_https_context

nltk.download()"""

import sys
import os
import logging
import shutil

sys.path.append('../')
from config import (PERSIST_DIRECTORY, DATA_FOLDER, CHUNK_SIZE, CHUNK_OVERLAP)

from langchain_community.document_loaders import UnstructuredMarkdownLoader
from langchain.text_splitter import RecursiveCharacterTextSplitter
from langchain.text_splitter import MarkdownHeaderTextSplitter

class DataLoader():
    """Create, load, save the DB using the confluence Loader"""
    def __init__(self):
        self.persist_directory = PERSIST_DIRECTORY
        self.data_folder = DATA_FOLDER
        self.chunk_size = int(CHUNK_SIZE)
        self.chunk_overlap = int(CHUNK_OVERLAP)
    

    def load_from_markdown_loader(self):
        """Load Markdown files from data folder"""
        docs = []

        for md_file in os.listdir(self.data_folder):
            if md_file.endswith(".md"):
                md_filepath = os.path.join(self.data_folder, md_file)

                # Load Markdown files
                md_doc_loader = UnstructuredMarkdownLoader(md_filepath)
                md_doc = md_doc_loader.load()[0]
                
                docs.append(md_doc)

        return docs

    def split_docs(self, docs):
        # Markdown
        headers_to_split_on = [
            ("#", "Titre 1"),
            ("##", "Sous-titre 1"),
            ("###", "Sous-titre 2"),
        ]

        markdown_splitter = MarkdownHeaderTextSplitter(headers_to_split_on=headers_to_split_on)

        # Split based on markdown and add original metadata
        md_docs = []
        for doc in docs:
            md_doc = markdown_splitter.split_text(doc.page_content)
            for i in range(len(md_doc)):
                md_doc[i].metadata = md_doc[i].metadata | doc.metadata
            md_docs.extend(md_doc)

        # RecursiveTextSplitter
        # Chunk size big enough
        splitter = RecursiveCharacterTextSplitter(
            chunk_size=self.chunk_size,
            chunk_overlap=self.chunk_overlap,
            separators=["\n\n", "\n", "(?<=\. )", " ", ""]
        )

        splitted_docs = splitter.split_documents(md_docs)
        #print("SPLITTED_DOCS[0].page_content : ", type(splitted_docs[0].page_content)) # --> <class 'str'>
        return splitted_docs

    def save_to_db(self, splitted_docs, embeddings):
        """Save chunks to Chroma DB"""
        from langchain_community.vectorstores import Chroma
        db = Chroma.from_documents(splitted_docs, embeddings, persist_directory=self.persist_directory)
        db.persist()

        return db

    def load_from_db(self, embeddings):
        """Loader chunks to Chroma DB"""
        from langchain.vectorstores import Chroma
        db = Chroma(
            persist_directory=self.persist_directory,
            embedding_function=embeddings
        )
        return db

    def set_db(self, embeddings):
        """Create, save, and load db"""
        try:
            shutil.rmtree(self.persist_directory) # if the directory exists it will be removed
        except Exception as e:
            logging.warning("%s", e)

        # Load docs
        docs = self.load_from_markdown_loader()

        # Split Docs
        splitted_docs = self.split_docs(docs)

        # Save to DB
        db = self.save_to_db(splitted_docs, embeddings)

        return db

    def get_db(self, embeddings):
        """Create, save, and load db"""
        db = self.load_from_db(embeddings)

        return db


if __name__ == "__main__":
    pass